import * as express from 'express';
import {
    ApiOperationDelete,
    ApiOperationGet,
    ApiOperationPatch,
    ApiOperationPost,
    ApiPath,
    SwaggerDefinitionConstant,
} from 'swagger-express-ts';
import {PostDto} from '@dtos';
import {NotificationClient, PostService} from '@services';
import {ENV, IErrorCode, KeycloakAuth} from '@shared';
import {CREATED, OK} from 'http-status-codes';
import {
    controller,
    httpDelete,
    httpGet,
    httpPatch,
    httpPost,
    interfaces,
    requestBody, requestHeaders,
    requestParam
} from 'inversify-express-utils';
import {body, param} from 'express-validator';
import {inject} from 'inversify';
import {ControllerHelper} from '@services';
import Controller = interfaces.Controller;
import {IncomingHttpHeaders} from 'http';

@ApiPath({
    path: '/posts',
    name: 'Controller',
})
@controller(
    '/posts',
    !(process.env.NODE_ENV || '').match(ENV.test) ? KeycloakAuth.getInstance().protect() : KeycloakAuth.getInstance().middleware(),
)
export class PostCtrl implements Controller {

    public static ERROR_DEFINE_ID_IS_UNAUTHORIZED = 'Vous ne devez pas indiquer l\'identifiant du postaire';
    public static ERROR_TITLE_REQUIRED_BETWEEN_1_AND_50 = 'Le titre est obligatoire et doit contenir 50 caractères maximum.';
    public static ERROR_MESSAGE_REQUIRED_BETWEEN_1_AND_255 = 'Le message est obligatoire et doit contenir 255 caractères maximum.';
    public static ERROR_BAD_ID = 'L\'identifiant n\'est pas valide.';

    constructor(
        @inject(PostService.name) private postService: PostService,
        @inject(NotificationClient.name) private notificationClient: NotificationClient,
        @inject(ControllerHelper.name) private controllerHelper: ControllerHelper,
    ) {}

    @ApiOperationGet({
        summary: 'Get all posts',
        description: 'Get posts of the post',
        responses: {
            200: {
                description: 'Success',
                type: SwaggerDefinitionConstant.ARRAY,
                model: 'Post',
            },
        },
    })
    @httpGet('/')
    public getAllPosts(req: express.Request, res: express.Response) {
        return this.postService.getAllPosts()
            .then((posts: PostDto[]) => res.status(OK).json(posts))
            .catch((err: IErrorCode) => this.controllerHelper.handleError(req, res, err));
    }

    @ApiOperationGet({
        path: '/{id}',
        summary: 'Get post by id',
        description: 'Get a post with its id',
        parameters: {
            path: {
                postId: {
                    type: SwaggerDefinitionConstant.STRING,
                    required: true,
                },
            },
        },
        responses: {
            200: {
                description: 'Success',
                type: SwaggerDefinitionConstant.OBJECT,
                model: 'Post',
            },
        },
    })
    @httpGet('/:id',
        param('id').isUUID(4).withMessage(PostCtrl.ERROR_BAD_ID),
    )
    public getPostById(@requestParam('id') id: string, req: express.Request, res: express.Response) {
        if (this.controllerHelper.errorWithParams(req, res)) { return res; }

        return this.postService.getPostById(id).then((post: PostDto) => {
            res.status(OK).json(post);
        }).catch((err: IErrorCode) => this.controllerHelper.handleError(req, res, err));
    }

    @ApiOperationPost({
        summary: 'Add a new post',
        description: 'Add a new post to the post',
        parameters: {
            body: {
                properties: {
                    title: {
                        type: SwaggerDefinitionConstant.STRING,
                        required: true,
                    },
                    content: {
                        type: SwaggerDefinitionConstant.STRING,
                        required: true,
                    },
                },
            },
        },
        responses: {
            201: { description: 'Created' },
        },
    })
    @httpPost('/',
        body('id').not().exists().withMessage(PostCtrl.ERROR_DEFINE_ID_IS_UNAUTHORIZED),
        body('title').isLength({ min: 1, max: 50 }).withMessage(PostCtrl.ERROR_TITLE_REQUIRED_BETWEEN_1_AND_50),
        body('content').isLength({ min: 1, max: 255 }).withMessage(PostCtrl.ERROR_MESSAGE_REQUIRED_BETWEEN_1_AND_255),
    )
    public addPost(
        @requestParam('id') id: string, @requestBody() post: PostDto,
        req: express.Request, res: express.Response,
    ) {
        if (this.controllerHelper.errorWithParams(req, res)) { return res; }

        const userId = (req as any).kauth.grant.access_token.content.sub;
        return this.postService.addPost(userId, post)
            .then((createdPost: PostDto) => {

                const authorization = req.headers.authorization || '';
                return this.notificationClient.push(authorization, post.title).then(() =>
                    res.header('location', `/posts/${createdPost.id}`).sendStatus(CREATED),
                ).catch((err: IErrorCode) => this.controllerHelper.handleError(req, res, err));
            })
            .catch((err: IErrorCode) => this.controllerHelper.handleError(req, res, err));
    }

    @ApiOperationPatch({
        path: '/{id}',
        summary: 'Edit a post',
        description: 'Edit a post of the post',
        parameters: {
            path: {
                postId: {
                    type: SwaggerDefinitionConstant.STRING,
                    required: true,
                },
            },
            body: {
                properties: {
                    content: {
                        type: SwaggerDefinitionConstant.STRING,
                        required: true,
                    },
                },
            },
        },
        responses: {
            200: { description: 'Success' },
        },
    })
    @httpPatch('/:id',
        param('id').isUUID(4).withMessage(PostCtrl.ERROR_BAD_ID),
        body('content').isLength({ min: 1, max: 255 }).withMessage(PostCtrl.ERROR_MESSAGE_REQUIRED_BETWEEN_1_AND_255),
    )
    public editPostById(
        @requestParam('id') id: string, @requestBody() post: PostDto,
        req: express.Request, res: express.Response,
    ) {
        if (this.controllerHelper.errorWithParams(req, res)) { return res; }

        const userId = (req as any).kauth.grant.access_token.content.sub;
        return this.postService.editPostById(id, userId, post)
            .then(() => res.sendStatus(OK))
            .catch((err: IErrorCode) => this.controllerHelper.handleError(req, res, err));
    }

    @ApiOperationDelete({
        path: '/{id}',
        summary: 'Delete a post',
        description: 'Delete a post of the post',
        parameters: {
            path: {
                postId: {
                    type: SwaggerDefinitionConstant.STRING,
                    required: true,
                },
            },
        },
        responses: {
            200: { description: 'Success' },
        },
    })
    @httpDelete('/:id',
        param('id').isUUID(4).withMessage(PostCtrl.ERROR_BAD_ID),
    )
    public deletePostById(@requestParam('id') id: string, req: express.Request, res: express.Response) {
        if (this.controllerHelper.errorWithParams(req, res)) { return res; }

        const userId = (req as any).kauth.grant.access_token.content.sub;
        return this.postService.deletePostById(id, userId).then(() => {
            res.sendStatus(OK);
        }).catch((err: IErrorCode) => this.controllerHelper.handleError(req, res, err));
    }

}
