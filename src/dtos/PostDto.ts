import {ApiModel, ApiModelProperty, SwaggerDefinitionConstant} from 'swagger-express-ts';

@ApiModel({
    name: 'Post',
})
export class PostDto {

    @ApiModelProperty({
        description: 'Id of post',
    })
    public id?: string;

    @ApiModelProperty({
        description: 'Title of post',
        required: true,
    })
    public title: string;

    @ApiModelProperty({
        description: 'Content of post',
        required: true,
    })
    public content: string;

    @ApiModelProperty({
        description: 'Author of post',
        required: true,
    })
    public authorId: string;

    @ApiModelProperty({
        description: 'Created date of post',
        type: SwaggerDefinitionConstant.STRING,
        format: 'date',
    })
    public createdAt?: Date;

    @ApiModelProperty({
        description: 'Updated date of post',
        type: SwaggerDefinitionConstant.STRING,
        format: 'date',
    })
    public updatedAt?: Date;

    constructor(title: string, content: string, authorId: string, id?: string) {
        this.id = id;
        this.title = title;
        this.content = content;
        this.authorId = authorId;
    }

}
