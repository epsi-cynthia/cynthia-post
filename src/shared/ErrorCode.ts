import {BAD_REQUEST, FORBIDDEN, INTERNAL_SERVER_ERROR, NOT_FOUND} from 'http-status-codes';

export interface IErrorCode {
    status: number;
    message: string;
    items?: any[];
}

export class ErrorCode {

    public static readonly INTERNAL_ERROR: IErrorCode = {
        status: INTERNAL_SERVER_ERROR,
        message: 'Une erreur inatendue est survenue.',
    };

    public static readonly BAD_FORMAT_TOKEN: IErrorCode = {
        status: BAD_REQUEST,
        message: 'Le format du token est incorrect.',
    };

    public static readonly NOT_FOUND: IErrorCode = {
        status: NOT_FOUND,
        message: 'Ressource introuvable.',
    };

    public static readonly BAD_REQUEST: IErrorCode = {
        status: BAD_REQUEST,
        message: 'Erreur des données transmises.',
    };

    public static readonly NO_POST: IErrorCode = {
        status: NOT_FOUND,
        message: 'Aucune publication ne correspond à cet identifiant',
    };

    public static readonly DONT_RIGHT_TO_EDIT: IErrorCode = {
        status: FORBIDDEN,
        message: 'Vous n\'avez pas les droits pour gérer cette publication',
    };

}
