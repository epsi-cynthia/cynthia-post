import cookieParser from 'cookie-parser';
import express from 'express';
import morgan from 'morgan';
import * as swagger from 'swagger-express-ts';
import {SwaggerService} from 'swagger-express-ts/swagger.service';
import swaggerUi from 'swagger-ui-express';
import {Container} from 'inversify';
import {interfaces, InversifyExpressServer, TYPE} from 'inversify-express-utils';
import {ErrorCode, KeycloakAuth} from '@shared';
import {HealthCtrl, PostCtrl} from '@controllers';
import {PostMapper} from '@mappers';
import {ControllerHelper, NotificationClient, PostService} from '@services';
import session from 'express-session';

// Set up container
const container = new Container();

container
    .bind<PostMapper>(PostMapper.name)
    .to(PostMapper)
    .inSingletonScope();
container
    .bind<ControllerHelper>(ControllerHelper.name)
    .to(ControllerHelper)
    .inSingletonScope();
container
    .bind<PostService>(PostService.name)
    .to(PostService);
container
    .bind<NotificationClient>(NotificationClient.name)
    .to(NotificationClient);
container
    .bind<interfaces.Controller>(TYPE.Controller)
    .to(HealthCtrl)
    .whenTargetNamed('Health');
container
    .bind<interfaces.Controller>(TYPE.Controller)
    .to(PostCtrl)
    .whenTargetNamed('Post');

// Init server
const server = new InversifyExpressServer(container);

server.setConfig((app) => {

    // Add middleware/settings/routes to express.
    app.use(morgan('dev'));
    app.use(express.json());
    app.use(express.urlencoded({extended: true}));
    app.use(cookieParser());

    // Setup CORS
    app.use((req, res, next) => {
        res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization');
        res.header('Access-Control-Allow-Origin', '*');
        res.header('Access-Control-Allow-Methods', 'OPTIONS, GET, POST, DELETE, PUT, PATCH');
        next();
    });

    // Create session
    app.use(session({
        secret: process.env.KC_SESSION_SECRET_KEY || '',
        resave: false,
        saveUninitialized: true,
        store: new session.MemoryStore()
    }));

    // Keycloak authentication
    app.use(KeycloakAuth.getInstance().middleware({
        logout: '/logout',
    }));

    // Generate swagger docs
    app.use(swagger.express({
        definition: {
            info: {
                title: 'Cynthia Post',
                version: '1.0',
            },
        },
    }));

    // Display swagger ui
    app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(
        SwaggerService.getInstance().getData(),
    ));

});

server.setErrorConfig((app: any) => {
    app.use((req: express.Request, res: express.Response, next: express.NextFunction) => {
        container.get<ControllerHelper>(ControllerHelper.name).handleError(req, res, ErrorCode.NOT_FOUND);
    });

    app.use((err: Error, req: express.Request, res: express.Response, next: express.NextFunction) => {
        console.log(err);
        const errorCode = err.message.includes('keycloak-token')
            ? ErrorCode.BAD_FORMAT_TOKEN
            : ErrorCode.INTERNAL_ERROR;

        container.get<ControllerHelper>(ControllerHelper.name).handleError(req, res, errorCode);
    });
});

// Export express instance
export default server;
