import {PostDto} from '@dtos';
import {PostModel} from '@models';
import {ErrorCode, logger} from '@shared';
import {PostMapper} from '@mappers';
import {inject, injectable} from 'inversify';

@injectable()
export class PostService {

    constructor(@inject(PostMapper.name) private postMapper: PostMapper) {
    }

    public getAllPosts(): Promise<PostDto[]> {
        return new Promise((resolve, reject) => {
            PostModel.findAll().then((posts) => resolve(
                posts.map((post) => this.postMapper.mapToDto(post)),
            )).catch((err) => {
                logger.error(err.stack);
                return reject(ErrorCode.INTERNAL_ERROR);
            });
        });
    }

    public getPostById(id: string): Promise<PostDto> {
        return new Promise((resolve, reject) => {
            PostModel.findByPk(id).then((post) => {
                if (!post) {
                    return reject(ErrorCode.NO_POST);
                }

                return resolve(this.postMapper.mapToDto(post));
            }).catch((err) => {
                logger.error(err.stack);
                return reject(ErrorCode.INTERNAL_ERROR);
            });

        });
    }

    public addPost(userId: string, post: PostDto): Promise<PostDto> {
        return new Promise((resolve, reject) => {
            post.authorId = userId;
            const postModel = new PostModel(post);
            postModel.save()
                .then((postCreated) => resolve(this.postMapper.mapToDto(postCreated)))
                .catch((err) => {
                    logger.error(err.stack);
                    return reject(ErrorCode.INTERNAL_ERROR);
                });
        });
    }

    public editPostById(id: string, userId: string, postDto: PostDto): Promise<boolean> {
        return new Promise((resolve, reject) => {
            PostModel.findByPk(id).then((post) => {
                if (!post) { return reject(ErrorCode.NO_POST); }

                if (post.authorId !== userId) { return reject(ErrorCode.DONT_RIGHT_TO_EDIT); }

                PostModel.update(postDto, {
                    where: { id },
                }).then((res) => {
                    if (res[0] === 0) { return reject(ErrorCode.NO_POST); }

                    return resolve(true);
                }).catch((err) => {
                    logger.error(err.stack);
                    return reject(ErrorCode.INTERNAL_ERROR);
                });

            }).catch((err) => {
                logger.error(err.stack);
                return reject(ErrorCode.INTERNAL_ERROR);
            });
        });
    }

    public deletePostById(id: string, userId: string): Promise<boolean> {
        return new Promise((resolve, reject) => {
            PostModel.findByPk(id).then((post) => {
                if (!post) { return reject(ErrorCode.NO_POST); }

                if (post.authorId !== userId) { return reject(ErrorCode.DONT_RIGHT_TO_EDIT); }


                PostModel.destroy({
                    where: {id},
                }).then((res) => {
                    if (!res) {
                        return reject(ErrorCode.NO_POST);
                    }
                    return resolve(true);
                }).catch((err) => {
                    logger.error(err.stack);
                    return reject(ErrorCode.INTERNAL_ERROR);
                });

            }).catch((err) => {
                logger.error(err.stack);
                return reject(ErrorCode.INTERNAL_ERROR);
            });
        });
    }

}
