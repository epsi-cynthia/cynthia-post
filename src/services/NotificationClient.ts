import * as request from 'request';
import {injectable} from 'inversify';
import {ErrorCode, logger} from '@shared';

@injectable()
export class NotificationClient {

    private readonly NOTIFICATION_URL = process.env.NOTIFICATION_URL;

    public push(authorization: string, message: string): Promise<boolean> {
        return new Promise<boolean>((resolve, reject) => {
            const options = {
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': authorization,
                },
                body: JSON.stringify({ message }),
            };

            request.post(this.NOTIFICATION_URL + '/notifications', options,
                (err: any, res: request.Response, body: any) => {
                if (err) {
                    logger.error(err);
                    if (res.statusCode === 400) { return reject(err); }

                    return reject(ErrorCode.INTERNAL_ERROR);
                }

                return resolve(true);
            });
        });
    }

}
