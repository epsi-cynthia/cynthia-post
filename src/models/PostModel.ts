import {Column, DataType, Length, Model, Default, PrimaryKey, Table} from 'sequelize-typescript';
import {mapTo} from '@wufe/mapper';
import {PostDto} from '@dtos';

@mapTo(PostDto)
@Table({
    tableName: 'post',
})
export class PostModel extends Model<PostModel> {
    @Default(DataType.UUIDV4)
    @PrimaryKey
    @Column(DataType.UUID)
    public id!: string;

    @Length({max: 50})
    @Column
    public title!: string;

    @Length({max: 255})
    @Column
    public content!: string;

    @Length({max: 255})
    @Column
    public authorId!: string;
}
