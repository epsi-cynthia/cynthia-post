import { Sequelize } from 'sequelize-typescript';
import * as models from './models';
import {logger} from '@shared';

export class SequelizeConnection {

    private static con: Sequelize;

    private static init(): void {
        SequelizeConnection.con = new Sequelize({
            host: process.env.DBHOST || '',
            port: Number(process.env.DBPORT),
            database: process.env.DBNAME || '',
            username: process.env.DBUSER || '',
            password: process.env.DBPASSWORD || '',
            dialect: 'postgres',
            dialectOptions: {
                connectTimeout: 2000,
            },
            define: {
                schema: process.env.DBSCHEMA,
            },
            timezone: '+02:00',
            logging: (sql) => logger.debug(sql),
            models: Object.values(models),
        });
    }

    public static getInstance(): Sequelize {
        if (!SequelizeConnection.con) {
            SequelizeConnection.init();
        }

        return SequelizeConnection.con;
    }

}
