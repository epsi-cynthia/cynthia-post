import {Mapper} from '@wufe/mapper';
import {PostModel} from '@models';
import {PostDto} from '@dtos';
import {injectable} from 'inversify';

@injectable()
export class PostMapper {

    private mapper?: Mapper;

    public mapToDto(post: PostModel): PostDto {
        return this.initializeMapper().map<PostModel, PostDto>(post);
    }

    private initializeMapper(): Mapper {
        if (!this.mapper) {
            this.mapper = new Mapper()
                .withConfiguration((configuration) => configuration.shouldIgnoreSourcePropertiesIfNotInDestination(true));

            this.mapper.createMap<PostModel, PostDto>(PostModel)
                .forMember('id', (opt) => opt.mapFrom((src) => src.id))
                .forMember('title', (opt) => opt.mapFrom((src) => src.title))
                .forMember('content', (opt) => opt.mapFrom((src) => src.content))
                .forMember('authorId', (opt) => opt.mapFrom((src) => src.authorId))
                .forMember('createdAt', (opt) => opt.mapFrom((src) => src.createdAt))
                .forMember('updatedAt', (opt) => opt.mapFrom((src) => src.updatedAt));
        }

        return this.mapper;
    }

}
