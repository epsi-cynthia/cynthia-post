# API
Micro-service Post

## Env
- _Node.js_ : 12.*
- _Express_ : 4.17.1
- _Sequelize_ : 5.21.0

## Run
- _Start in prod_ : npm start
- _Start in dev_ : npm run dev
- _Start tests_ : npm run test
