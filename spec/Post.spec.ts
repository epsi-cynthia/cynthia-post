import server from '@server';
import supertest from 'supertest';

import { Response, SuperTest, Test } from 'supertest';
import {CREATED, OK} from 'http-status-codes';
import {SequelizeConnection} from '../src/SequelizeConnection';
import {PostDto} from '@dtos';
import {PostModel} from '@models';

describe('Comment', () => {

    let agent: SuperTest<Test>;

    // Comment added in startup in database
    const firstPost: PostDto = {
        id: '4fb65bf8-bce8-4402-b1db-0007a43f52e9',
        title: 'test1',
        content: 'test1',
        authorId: '22470205-ad46-43f4-abee-9df20b9b2333',
    };

    // Comment to create
    const secondPost: PostDto = {
        title: 'test2',
        content: 'test2',
        authorId: '22470205-ad46-43f4-abee-9df20b9b2333',
    };

    beforeAll(async (done) => {
        await SequelizeConnection.getInstance().sync();

        // Remove all and re-add
        PostModel.destroy({
            where: {},
        }).then(() => {
            const postModel = new PostModel(firstPost);

            postModel.save().then((createdPost) => {

                firstPost.createdAt = createdPost.createdAt;
                firstPost.updatedAt = createdPost.updatedAt;

                agent = supertest.agent(server.build());
                done();

            }).catch(console.log);
        }).catch(console.log);
    });

    it('Get all posts', (done) => {
        return agent.get(`/posts`)
            .expect(OK)
            .end((err: Error, res: Response) => {
                expect(res.body.length).toBeGreaterThan(0);
                done();
            });
    });

    it('Get one post', (done) => {
        return agent.get(`/posts/${firstPost.id}`)
            .expect(OK)
            .end((err: Error, res: Response) => {
                expect(res.body.id).toBe(firstPost.id);
                done();
            });
    });

    it('Add a new post', (done) => {
        return agent.post(`/posts/`)
            .type('json')
            .send({
                title: secondPost.title,
                content: secondPost.content,
                authorId: secondPost.authorId,
            })
            .expect(CREATED)
            .end((err: Error, res: Response) => {
                if (res.header.location) {
                    const location = String(res.header.location).split('/');
                    secondPost.id = location[location.length - 1];
                }
                done();
            });
    });

    it('Edit a post', () => {
        return agent.patch(`/posts/${firstPost.id}`)
            .type('json')
            .send({
                content: 'Edited content',
            })
            .expect(OK);
    });

    it('Delete a post', () => {
        return agent.delete(`/posts/${firstPost.id}`)
            .expect(OK);
    });

});
