import {SequelizeConnection} from '../src/SequelizeConnection';
import {PostModel} from '@models';
import {PostMapper} from '@mappers';
import {PostDto} from '@dtos';

describe('Mapper', () => {

    const uuid = '7de29744-872a-4396-bd03-27a0409779c7';

    it('Map PostModel to PostDto', async (done) => {
        await SequelizeConnection.getInstance().sync();

        // Unit test
        const postModel = new PostModel({
            id: uuid,
            title: 'test',
            content: 'test',
            authorId: uuid,
        });
        const postDto = new PostMapper().mapToDto(postModel);

        expect(postDto).toEqual(new PostDto('test', 'test', uuid, uuid));
        done();
    });

});
